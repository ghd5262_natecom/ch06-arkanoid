﻿#pragma strict
static var speed = 5.5;
static var undeadTime = 0.0;
private var pos : Vector3;

function Update () {
	if(jsGameManager.state == STATE.READY || jsGameManager.state == STATE.RESET){
		return;
	}
	pos = transform.position;
	var amtMove = Time.deltaTime * speed;
	transform.Translate(transform.forward * amtMove, Space.World);
	if(transform.position.z < -12 || transform.position.z > 2){
		jsGameManager.state = STATE.OUT;
	}
	
	undeadTime -= Time.deltaTime;
	if(undeadTime > 0)
		transform.renderer.material.color = Color.red;
	else
		transform.renderer.material.color = Color.white;
}

function OnTriggerEnter(coll : Collider){
	var flag = false;
	if(undeadTime > 0) flag = true;
	
	if(coll.tag == "FENCE" || coll.tag == "TOPFENCE"){
		CheckBounds(coll);
		coll.gameObject.audio.Play();
	}
	else if(coll.gameObject.tag.Substring(0, 5) == "BLOCK"){
		CheckBounds(coll);
		if(jsGameManager.state != STATE.DEMO)
			coll.gameObject.SendMessage("SetCollision", flag, SendMessageOptions.DontRequireReceiver);
	}
}


function CheckBounds(coll : Collider){
	if(coll.tag == "TOPFENCE"){
		transform.rotation.eulerAngles.y = 180 - transform.rotation.eulerAngles.y;
		return;
	}
	
	if(coll.tag == "FENCE"){
		transform.rotation.eulerAngles.y = 360 - transform.rotation.eulerAngles.y;
		return;
	}
	if(undeadTime > 0)return;
	
	var bounds : Bounds = coll.bounds;
	var w = transform.localScale.x / 2;
	
	if(pos.x > (bounds.min.x - w) && pos.x < (bounds.max.x + w)){
		transform.rotation.eulerAngles.y = 180 - transform.rotation.eulerAngles.y;
		transform.position = pos;
	}
	else{
		transform.rotation.eulerAngles.y = 360 - transform.rotation.eulerAngles.y;
		transform.position = pos;
	}
}