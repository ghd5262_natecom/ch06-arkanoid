﻿#pragma strict
private var speed = 4;

function Update () {
	var amtToMove = speed * Time.deltaTime;
	transform.Translate(Vector3.back * amtToMove);
	if(transform.position.z < -12)
	{
		Destroy(gameObject);
	}
}

function OnTriggerEnter(coll : Collider){
	if(coll.gameObject.tag == "PADDLE"){
		jsGameManager.bonusNum = int.Parse(gameObject.tag.Substring(5, 1));
		Destroy(gameObject);
	}
}